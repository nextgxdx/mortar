conf = (dir = { root: 'app', dest: 'temp', deploy: 'deploy' }) ->
  dir.root = "#{dir.root}"
  dir.dest = "build/#{dir.dest}/"
  dir.deploy = "build/#{dir.deploy}"

  c =
    "mortar:jade":
      src: "#{dir.root}/index.jade"
      dst: "#{dir.dest}/"

    "mortar:clean":
      src: [dir.dest, dir.deploy]

    "mortar:concat":
      src:
        coffee: [ "#{dir.root}/**/*.coffee", "!#{dir.root}/**/test/**/*" ]
        templates: "#{dir.root}/scripts/**/*.jade"
      dst: dir.dest

    "mortar:stylus":
      src: "#{dir.root}/styles/app.styl"
      dst: "#{dir.dest}/styles"

    "mortar:copy":
      src:
        data: "#{dir.root}/data/**/*"
        libs: "#{dir.root}/lib/**/*.js"
        images: "#{dir.root}/images/**/*"
      dst:
        data: [ "#{dir.dest}data/", "#{dir.deploy}/data/" ]
        libs: "#{dir.dest}/lib"
        images: "#{dir.dest}/images"

    "mortar:serve":
      name: 'DEFAULT'
      base: dir.dest
      port: 9000

    "mortar:watch":
      src: ['app/**/*.coffee', 'app/**/*.jade']
      stylus: 'app/**/*.styl'
      lib: 'app/lib/**/*'

    "mortar:revall":
      src: ["#{dir.dest}/**/*", "!#{dir.dest}/data/**/*"]
      cdn: '//d19ro8c33wywio.cloudfront.net/'
      dst: "#{dir.deploy}/"

    "mortar:version":
      version: ''
      dst: "#{dir.deploy}/version.html"

    "mortar:s3":
      branch: ''
      pkg: {}
      src:
        html: "#{dir.deploy}/*.html"
        data: "#{dir.deploy}/data/**.*"
        assets: [
          "#{dir.deploy}/**/*"
          "!#{dir.deploy}/**/*.html"
          "!#{dir.deploy}/data/**/*"
          "!#{dir.deploy}/scripts"
          "!#{dir.deploy}/**/_*"
        ]
      deploy_path: ''
      s3:
        cdn: undefined
        assets: undefined
        app: undefined
      headers:
        html:
          'Cache-Control': 'no-cache, must-revalidate'
          'Content-Type': 'text/html'
          'Content-Encoding': 'gzip'
        assets:
          'Cache-Control': 'max-age=15552000, public'
        data:
          'Cache-Control': 'no-cache, must-revalidate'
          'Content-Type': 'application/json'
          'Content-Encoding': 'gzip'
    dir: dir

  return c

module.exports = conf
