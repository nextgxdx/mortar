# mortar changelog

## 1.0.8

- Merge config properly

## 1.0.7

- Add mortar:test task as well as include data in copy tasks

## 1.0.6

- Fix issues with templatecache not getting revved

## 1.0.5

- Make changes to config

## 1.0.4 (9/26/14)

- Use gulp-notify
- Fix issues with deploying next branch
- update to gulp-stylus 1.3 to ensure nxt builds consistently

## 1.0.3 (8/4/14)

- Use package name in path

## 1.0.2 (7/16/14)

- Specify Gulp as a peer dependency to ensure mortar is using the parent projects gulp object.

## 1.0.1 (6/27/14)

- Fixed a path issue

## 1.0.0 (6/23/14)

- Initial Release
