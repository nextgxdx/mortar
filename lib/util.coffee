_ = require 'lodash'

util = {}

util.cdn = (branch) ->
  return '//d3806m6vn2o1ma.cloudfront.net/' if branch is 'master'
  return '//d19ro8c33wywio.cloudfront.net/'

util.assets = (branch) ->
  return 'ngd-assets' if branch is 'master'
  return 'ngd-assets-test'

util.app = (branch) ->
  return 'ngd-apps' if branch is 'master'
  return 'ngd-apps-next' if branch is 'next'
  return 'ngd-apps-test'

util.get_deploy_path = (branch, package_name) ->
  return "apps/#{package_name}" if branch is 'master' or branch is 'next'
  return "#{package_name}/#{branch}/"

util.validate = (config, props) ->
  prop = _.find props, (prop) ->
    !_.get(config, prop)?

  throw new Error("#{prop} is undefined.") if prop?
  return

module.exports = util
