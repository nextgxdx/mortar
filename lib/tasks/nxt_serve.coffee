
connect = require 'connect'
fs = require 'fs'
http = require 'http'
path = require 'path'
proxy = require 'proxy-middleware'
util = require 'gulp-util'


redirect = ({ root }) ->
  (req, res, next) ->
    return next() if req.url isnt '/'
    res.statusCode = 303
    res.setHeader 'Location', root
    res.end()


check_file = ({ base, root }) ->
  connect_static = connect.static base

  return (req, res, next) ->
    url = req.url.replace root, ''
    fs.exists path.join(base, url), (exists) ->
      return next() if not exists
      return connect_static req, res, next


rewrite_root = ({ base, root}) ->
  connect_static = connect.static base

  (req, res, next) ->
    return next() if not ~req.url.indexOf root

    req.url = req.url.replace root, ''
    req.url = '/' if not req.url

    connect_static req, res, next

rewrite_path = ({ pattern, path })->
  (req, res, next) ->
    return next() if !pattern? or !path?
    req.url = req.url.replace pattern, path
    next()

module.exports = exports = (options = {}) ->
  return ->
    app = connect()

    options.port ?= 3000
    options.root ?= "/apps/#{options.name}/"
    options.base ?= 'temp/'
    options.proxy ?= 'https://test.nextgxdx.com/'
    options.middlewares ?= []
    options.replace ?= {}

    proxy_options = require('url').parse options.proxy
    proxy_options.route = '/'
    proxy_options.cookieRewrite = true


    options.middlewares = options.middlewares.concat [
      redirect options
      check_file options
      rewrite_root options
      rewrite_path options.replace
      proxy proxy_options
    ]

    options.middlewares.forEach (middleware) ->
      app.use middleware

    http.createServer app
    .listen options.port, ->
      util.log util.colors.bgGreen "Server started on #{options.port} port"
