remember = require 'gulp-remember'
cache = require 'gulp-cached'
plumber = require 'gulp-plumber'
notify = require 'gulp-notify'
jade = require 'gulp-jade'
stylus = require 'gulp-stylus'
angularFilesort = require 'gulp-angular-filesort'
angularTemplatecache = require 'gulp-angular-templatecache'
coffee = require 'gulp-coffee'
awspublish = require 'gulp-awspublish'
rename = require 'gulp-rename'
combine = require 'stream-combiner2'

util =
  handle: (task) ->
    plumber
      errorHandler: notify.onError
        title: task
        message: (err) ->
          return err

pipes = {}

pipes.jade = ->
  combine.obj(
    util.handle('jade')
    jade()
  )

pipes.stylus = ->
  combine.obj(
    util.handle('stylus')
    stylus(compress: true)
  )

pipes.coffee = ->
  combine.obj(
    util.handle('coffee')
    cache('coffee')
    coffee()
    remember('coffee')
    angularFilesort()
  )

pipes.templates = ->
  combine.obj(
    cache('templates')
    jade()
    remember('templates')
    angularTemplatecache
      module: 'app.templates'
      standalone: true
      root: '/scripts/'
    util.handle('templates')
  )

pipes.s3 = (path, headers, publisher) ->
  combine.obj(
    rename(path)
    awspublish.gzip()
    publisher.publish(headers)
    awspublish.reporter()
  )

module.exports = pipes
