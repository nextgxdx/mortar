util = require 'util'
_ = require 'lodash'
Undertaker = require 'undertaker'
DefaultRegistry = require 'undertaker-registry'
DEFAULT_CONFIG = require('../../mortar.conf')()

ConfigRegistry = (config) ->
  DefaultRegistry.call(this)
  this.config = _.merge DEFAULT_CONFIG, config
  return this

util.inherits ConfigRegistry, DefaultRegistry

ConfigRegistry::set = (name, fn) ->
  self = this
  this.config[name] ?= {}
  this.config[name].globals = -> _.clone self.config
  task = this._tasks[name] = fn.bind _.clone this.config[name]
  task.displayName = name
  return task

module.exports = ConfigRegistry
