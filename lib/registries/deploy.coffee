gulp = require 'gulp'
path = require 'path'
fs = require 'fs'
awspublish = require 'gulp-awspublish'
revAll = require 'gulp-rev-all'
rename = require 'gulp-rename'
filter = require 'gulp-filter'
uglify = require 'gulp-uglify'
merge = require 'merge-stream'
pipes = require '../pipes'
util = require '../util'

DeployRegistry = (taker) ->
  taker.task 'mortar:revall', ->
    self = this

    # Uglify takes a long time
    js_filter = filter(["**/*.js"])

    gulp.src self.src
      .pipe js_filter
      .pipe uglify()
      .pipe js_filter.restore()
      .pipe revAll
        ignore: ['.html']
        transformPath: (rev, source) ->
          return self.cdn + path.basename(rev)
      .pipe gulp.dest self.dst

  taker.task 'mortar:version', (cb) ->
    fs.writeFile this.dst, this.version, cb

  taker.task 'mortar:s3:html', ->
    self = this

    s3_config = self.globals()['mortar:s3']
    util.validate(s3_config, ['s3.cdn', 's3.app', 's3.assets'])

    default_publisher = awspublish.create bucket: s3_config.s3.app, profile: s3_config.aws_profile

    html_pipe = pipes.s3
      dirname: s3_config.deploy_path
      extname: ''
      s3_config.headers.html
      default_publisher

    gulp.src s3_config.src.html
      .pipe html_pipe

  taker.task 'mortar:s3:data', ->
    self = this

    s3_config = self.globals()['mortar:s3']
    util.validate(s3_config, ['s3.cdn', 's3.app', 's3.assets'])

    default_publisher = awspublish.create bucket: s3_config.s3.app, profile: s3_config.aws_profile

    data_pipe = pipes.s3
      dirname: "#{s3_config.deploy_path}/data"
      s3_config.headers.data
      default_publisher

    gulp.src s3_config.src.data
      .pipe data_pipe

  taker.task 'mortar:s3:assets', ->
    self = this

    s3_config = self.globals()['mortar:s3']
    util.validate(s3_config, ['s3.cdn', 's3.app', 's3.assets'])

    asset_publisher = awspublish.create bucket: s3_config.s3.assets, profile: s3_config.aws_profile

    asset_pipe = pipes.s3
      dirname: ''
      s3_config.headers.assets
      asset_publisher

    gulp.src s3_config.src.assets
      .pipe asset_pipe

  taker.task 'mortar:s3',
    taker.series('mortar:s3:assets', 'mortar:s3:data', 'mortar:s3:html')

  taker.task 'mortar:deploy',
    taker.series('mortar:build', 'mortar:revall', 'mortar:version', 'mortar:s3')

  return taker.registry()

module.exports = DeployRegistry
