gulp = require 'gulp'
merge = require 'merge2'
path = require 'path'
clean = require 'gulp-clean'
concat = require 'gulp-concat'
serve = require '../tasks/nxt_serve'
pipes = require '../pipes'
livereload = require 'gulp-livereload'
gutil = require 'gulp-util'
ngAnnotate = require 'gulp-ng-annotate'

CommonRegistry = (taker) ->
  taker.task 'mortar:clean', ->
    gulp.src this.src, read: false
      .pipe clean()

  taker.task 'mortar:concat', ->
    self = this
    coffee = ->
      gulp.src self.src.coffee
        .pipe pipes.coffee()
        .on 'error', gutil.log

    templates = ->
      gulp.src self.src.templates
        .pipe pipes.templates()
        .on 'error', gutil.log

    merge(templates(), coffee())
      .pipe concat 'app.js'
      .pipe ngAnnotate()
      .pipe gulp.dest this.dst

  taker.task 'mortar:jade', ->
    gulp.src this.src
      .pipe pipes.jade()
      .pipe gulp.dest this.dst
      .on 'error', gutil.log

  taker.task 'mortar:stylus', ->
    gulp.src this.src
      .pipe pipes.stylus()
      .pipe gulp.dest this.dst
      .on 'error', gutil.log

  taker.task 'mortar:copy', ->
    libs = gulp.src this.src.libs
            .pipe gulp.dest this.dst.libs

    images = gulp.src this.src.images
              .pipe gulp.dest this.dst.images

    data = gulp.src this.src.data
            .pipe gulp.dest this.dst.data[0]
            .pipe gulp.dest this.dst.data[1]

    merge libs, images, data

  taker.task 'mortar:serve', -> serve(this)()

  taker.task 'mortar:build',
    taker.parallel('mortar:jade', 'mortar:stylus', 'mortar:copy', 'mortar:concat')

  taker.task 'mortar:reload', (done) ->
    livereload.reload()
    done()

  taker.task 'mortar:watch:after', (cb) -> cb()

  taker.task 'mortar:watch', ->
    self = this
    livereload.listen()

    gulp.watch self.src,
      taker.series 'mortar:jade', 'mortar:concat', 'mortar:reload', 'mortar:watch:after'

    gulp.watch self.stylus,
      taker.series 'mortar:stylus', 'mortar:reload', 'mortar:watch:after'

    gulp.watch self.lib,
      taker.series 'mortar:copy', 'mortar:reload', 'mortar:watch:after'

  return taker.registry()

module.exports = CommonRegistry
