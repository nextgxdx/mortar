/**
 * @preserve AngularJS PDF viewer directive using pdf.js.
 *
 * https://github.com/akrennmair/ng-pdfviewer
 *
 * MIT license
 */

angular.module('ngPDFViewer', []).
directive('pdfviewer', [ '$parse', function($parse) {
	return {
		restrict: "CE",
		transclude: true,
		template: '<canvas></canvas>',
		scope: {
			onPageLoad: '&',
			loadProgress: '&'
		},
		controller: [ '$scope', '$element', '$transclude', function($scope, $element, $transclude) {
			var canvas = $element.find('canvas')[0];

			$scope.pageNum = 1;
			$scope.pdfDoc = null;
			$scope.scale = 1.0;

			$scope.documentProgress = function(progressData) {
				if ($scope.loadProgress) {
					$scope.loadProgress({state: "loading", loaded: progressData.loaded, total: progressData.total});
				}
			};

			$scope.loadPDF = function(path) {
				console.log('loadPDF ', path);
				PDFJS.getDocument(path, null, null, $scope.documentProgress).then(function(_pdfDoc) {
					$scope.pdfDoc = _pdfDoc;
					$scope.renderPage($scope.pageNum, function(success) {
						if ($scope.loadProgress) {
							$scope.loadProgress({state: "finished", loaded: 0, total: 0});
						}
					});
				}, function(message, exception) {
					console.log("PDF load error: " + message);
					if ($scope.loadProgress) {
						$scope.loadProgress({state: "error", loaded: 0, total: 0});
					}
				});
			};

			$scope.renderPage = function(num, callback) {
				console.log('renderPage ', num);
				$scope.pdfDoc.getPage(num).then(function(page) {
					var viewport = page.getViewport($scope.scale);
					var ctx = canvas.getContext('2d');

					canvas.height = viewport.height;
					canvas.width = viewport.width;

					page.render({ canvasContext: ctx, viewport: viewport }).promise.then(
						function() {
							if (callback) {
								callback(true);
							}
							$scope.$apply(function() {
								$scope.onPageLoad({ page: $scope.pageNum, total: $scope.pdfDoc.numPages });
							});
						},
						function() {
							if (callback) {
								callback(false);
							}
							console.log('page.render failed');
						}
					);
				});
			};

			$scope.gotoPage = function(page) {
				if (page >= 1 && page <= $scope.pdfDoc.numPages) {
					$scope.pageNum = page;
					$scope.renderPage($scope.pageNum);
				}
			};

			$scope.nextPage = function() {
				$scope.gotoPage($scope.pageNum + 1);
			};

			$scope.prevPage = function() {
				$scope.gotoPage($scope.pageNum - 1);
			};

			$scope.$on('pdfviewer.nextPage', function(evt, id) {
				if (id === $scope.id) {
					return $scope.nextPage();
				}
			});

			$scope.$on('pdfviewer.prevPage', function(evt, id) {
				if (id === $scope.id) {
					return $scope.prevPage();
				}
			});

			$scope.$on('pdfviewer.gotoPage', function(evt, id, page) {
				if (id === $scope.id) {
					return $scope.gotoPage(page);
				}
			});

			$transclude(function(clone, tScope) {
				angular.extend(tScope, {
					nextPage: $scope.nextPage,
					prevPage: $scope.prevPage,
					gotoPage: $scope.gotoPage
				});

				$element.append(clone);
			});

		} ],
		link: function(scope, iElement, iAttr) {
			iAttr.$observe('id', function(id) {
				scope.id = id;
			});

			iAttr.$observe('src', function(v) {
				console.log('src attribute changed, new value is', v);
				if (v !== undefined && v !== null && v !== '') {
					scope.pageNum = 1;
					scope.loadPDF(v);
				}
			});
		}
	};
}]).
service("PDFViewerService", [ '$rootScope', function($rootScope) {

	var svc = { };
	svc.nextPage = function() {
		$rootScope.$broadcast('pdfviewer.nextPage');
	};

	svc.prevPage = function() {
		$rootScope.$broadcast('pdfviewer.prevPage');
	};

	svc.Instance = function(id) {
		var instance_id = id;

		return {
			prevPage: function() {
				$rootScope.$broadcast('pdfviewer.prevPage', instance_id);
			},
			nextPage: function() {
				$rootScope.$broadcast('pdfviewer.nextPage', instance_id);
			},
			gotoPage: function(page) {
				$rootScope.$broadcast('pdfviewer.gotoPage', instance_id, page);
			}
		};
	};

	return svc;
}]);
