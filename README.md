# Mortar

## Usage


    gulp = require 'gulp'
    path = require 'path'
    pkg = require './package.json'

    config =
      "mortar:s3":
        pkg: require './package.json'
        s3:
          cdn: '//s3.amazonaws.com/assets.mortar.test.bucket/'
          assets: 'assets.mortar.test.bucket'
          app: 'mortar.test.bucket'
        deploy_path: "#{pkg.name}/branch/"
      "mortar:version":
        version: '1234'
      "mortar:revall":
        cdn: '//s3.amazonaws.com/assets.mortar.test.bucket/'
      "mortar:serve":
        name: pkg.name

    Mortar = require '../index'
    mortar = new Mortar(config)

    gulp.registry mortar.common

    gulp.task 'default',
      gulp.series('mortar:build', gulp.parallel('mortar:serve', 'mortar:watch'))


# Example project
An example project is included in `example/`
