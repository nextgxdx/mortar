Undertaker = require 'undertaker'
ConfigRegistry = require './lib/registries/config'
CommonRegistry = require './lib/registries/common'
DeployRegistry = require './lib/registries/deploy'
util = require './lib/util'
taker = new Undertaker()

Mortar = (config) ->
  this.config = config
  this.taker = new Undertaker()
  this.taker.registry new ConfigRegistry(config)
  new CommonRegistry(this.taker)
  new DeployRegistry(this.taker)

  return this

Mortar.util = util

module.exports = Mortar
